const fs = require('fs');
const os = require('os');
const http = require('http')
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const user = os.userInfo().username;

const message = `
Choose an option:
1. Read my package.json
2. Read my OS info
3. Start a http server
-----------------------
Type a number: `;

const osInfo = `
Getting OS info...
SYSTEM MEMORY: ${(os.totalmem() / 1024 / 1024 / 1024 ).toFixed(2) + 'GB'}
FREE MEMORY: ${(os.freemem() / 1024 / 1024 / 1024 ).toFixed(2) + 'GB'}
CPU CORES: ${os.cpus().length}
ARCH: ${os.arch()}
PLATFORM: ${os.platform()}
RELEASE: ${os.release()}
USER: ${user}`;


rl.question(message, (answer) => {

    if (answer == 1) {
        console.log('Reading package.json file')
        fs.readFile( __dirname + '/package.json', 'utf-8', (err, content) => {
            console.log(content);
        });
    } 

    else if (answer == 2) {
        console.log(osInfo);
    } 

    else if (answer == 3) {
        console.log("Stating HTTP server...")
        console.log("Server listening on port 1337.")
        const server = http.createServer((req, res) => {
            
            if (req.url === '/') {
                res.write(`Hello ${user} and welcome to the server... `);
                res.end();
            }

            if (req.url === '/task-17') {
                res.write(JSON.stringify( ['JavaScript', 'Node.JS'] ));
                res.end();
            }

        });

        server.listen(1337);

    } else {
        console.log('Invalid option.')
    }
    
    rl.close();
});